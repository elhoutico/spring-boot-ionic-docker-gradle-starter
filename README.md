This project is intended to help build an ionic app backed by a spring server, docker is optional and here only to help 
 for deployment in production
# Getting started
the information below is intended to help getting started in a DEV environment.

The project is divided into two parts:
- The frontend folder containing ionic files 
- The src folder containing spring files

## frontend
to run the fronted:
```
cd frontend
npm install #to install all the dependencies
ionic serve
```

##backend
to run the backend use the main method inside application.java using your favorite IDE.

# design decisions
The project is intended to be build in a modular way
## frontend
to add a module in the frontend, the easiest way is to copy the testModule and import it manually inside app.module.ts.

you can also use ionic g page module_name, but you would need to:
- remove references to navController
- add the module_name.routing.module.ts
- import the added RoutingModule into module_name.module.ts

to keep the code clean you **SHOULD NOT**:
- use the ionic navController and ionic navigation events...
- reference a module variable inside another
- modify the app folder (except from the import module)

##backend
to add a module in the backend just add a folder with the module_name inside the com/elhouti/starter/modules folder next to the test folder
