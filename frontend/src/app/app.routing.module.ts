import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {HomePage} from "../pages/home/home";

export const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component:HomePage },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
