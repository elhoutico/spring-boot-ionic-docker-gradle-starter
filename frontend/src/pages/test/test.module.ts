import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestPage } from './test';
import {TestRoutingModule} from "./test.routing.module";

@NgModule({
  declarations: [
    TestPage,
  ],
  imports: [
    IonicPageModule.forChild(TestPage),
    TestRoutingModule
  ],
  exports: [
    TestPage
  ]
})
export class TestPageModule {}
